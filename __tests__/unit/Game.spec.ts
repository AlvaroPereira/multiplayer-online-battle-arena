import Game from "../../src/domain/Game";
import Player from "../../src/domain/Player";
import crypto from "crypto";
import Arena from "../../src/domain/Arena";

describe("Game", () => {
    test("Should create a new game when game was instantiated", () => {
        let game: Game = new Game();
        let playerOne: Player = new Player();
        let playerTwo: Player = new Player();
        const arena: Arena = new Arena();

        const playerOneId: string = crypto.randomUUID();
        const playerOneName: string = crypto.randomBytes(16).toString("hex");
        const playerTwoId: string = crypto.randomUUID();
        const playerTwoName: string = crypto.randomBytes(16).toString("hex");
        const arenaId: string = crypto.randomUUID();
        const arenaName: string = crypto.randomBytes(16).toString("hex");
        const arenaWidth: number = 10;
        const arenaHeight: number = 20;

        playerOne.id = playerOneId;
        playerOne.name = playerOneName;

        playerTwo.id = playerTwoId;
        playerTwo.name = playerTwoName;

        arena.id = arenaId;
        arena.name = arenaName;
        arena.width = arenaWidth;
        arena.height = arenaHeight;

        game.playerOne = playerOne;
        game.playerTwo = playerTwo;
        game.arena = arena;

        expect(playerOne).toBeInstanceOf(Player);
        expect(playerTwo).toBeInstanceOf(Player);
        expect(game.playerOne.id).toStrictEqual(playerOneId);
        expect(game.playerOne.name).toStrictEqual(playerOneName);
        expect(game.playerTwo.id).toStrictEqual(playerTwoId);
        expect(game.playerTwo.name).toStrictEqual(playerTwoName);
        expect(game.arena.id).toStrictEqual(arenaId);
        expect(game.arena.name).toStrictEqual(arenaName);
        expect(game.arena.width).toStrictEqual(arenaWidth);
        expect(game.arena.height).toStrictEqual(arenaHeight);
    })
})