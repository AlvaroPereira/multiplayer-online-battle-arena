import Structure from "../../src/domain/Structure";

describe("Structure", () => {

  test("Should create a new structure when structure was instantiated", () => {

    let structure: Structure = new Structure();

    structure.isVisible = true;
    structure.isDestructible = true;
    structure.isDestructed = true;
    structure.healthPoints = 100;
    structure.canAttack = true;
    structure.isCollisional = true;
    structure.x = 5;
    structure.y = 10;
    
    expect(structure.isVisible).toBe(true);
    expect(structure.isDestructible).toBe(true);
    expect(structure.isDestructed).toBe(true);
    expect(structure.healthPoints).toBe(100);
    expect(structure.canAttack).toBe(true);
    expect(structure.isCollisional).toBe(true);
    expect(structure.x).toBe(5);
    expect(structure.y).toBe(10);
  });
});