import * as crypto from "crypto";
import Arena from "../../src/domain/Arena";

describe("Arena", () => {
    test("Should create arena object when Arena was called", () => {
        const arenaId: string = crypto.randomUUID();
        const arenaName: string = crypto.randomBytes(16).toString("hex");
        const arenaWidth: number = 10;
        const arenaHeight: number = 20;

        let arena: Arena = new Arena();
        arena.id = arenaId;
        arena.name = arenaName;
        arena.width = arenaWidth;
        arena.height = arenaHeight;

        expect(arena).toBeInstanceOf(Arena);
        expect(arena.id).toStrictEqual(arenaId);
        expect(arena.name).toStrictEqual(arenaName);
        expect(arena.width).toStrictEqual(arenaWidth);
        expect(arena.height).toStrictEqual(arenaHeight);
    })
});