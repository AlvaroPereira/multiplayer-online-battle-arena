import * as crypto from "crypto";
import Player from "../../src/domain/Player";

describe('PLayer', () => {
    test("Should create player object when Player was called", () => {
        const playerId: string = crypto.randomUUID();
        const playerName: string = crypto.randomBytes(16).toString("hex");

        let player: Player = new Player();
        player.id = playerId;
        player.name = playerName;

        expect(player).toBeInstanceOf(Player);
        expect(player.id).toStrictEqual(playerId);
        expect(player.name).toStrictEqual(playerName);
    })
});