export default class Arena {
    public id: string | undefined;
    public name: string | undefined;
    public width: number | undefined;
    public height: number | undefined;
}