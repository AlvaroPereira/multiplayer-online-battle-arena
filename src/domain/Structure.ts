export default class Structure {
    public isVisible?: boolean = false;
    public isDestructible?: boolean = false;
    public isDestructed?: boolean = false;
    public healthPoints: number = 0;
    public canAttack?: boolean = false;
    public isCollisional: boolean = false;
    public x: number = 0;
    public y: number = 0;
}