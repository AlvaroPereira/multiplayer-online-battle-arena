import Player from "./Player";
import Arena from "./Arena";

export default class Game {
    public playerOne: Player | undefined;
    public playerTwo: Player | undefined;
    public arena: Arena | undefined;
}